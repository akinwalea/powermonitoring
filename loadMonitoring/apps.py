from django.apps import AppConfig


class LoadmonitoringConfig(AppConfig):
    name = 'loadMonitoring'
