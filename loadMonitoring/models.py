from django.db import models

# Create your models here.
class Transformer(models.Model):
    power_rating =  models.IntegerField(default=0) 
    name = models.CharField(max_length=200) 
    condition = models.CharField(max_length=200) 
    service_type = models.CharField(max_length=200) 
    life_span = models.IntegerField(default=0)
    created_at =  models.DateTimeField('date published')

class Meter(models.Model):
    customer_name = models.CharField(max_length=200) 
    transformer = models.ForeignKey(Transformer, on_delete=models.CASCADE) 
    category = models.CharField(max_length=200) 
    energy_demand =  models.DecimalField(max_digits=10, decimal_places=2)
