from django.conf.urls import url
from . import views
from django.contrib.auth.views import login

urlpatterns = [
    url(r'^$', login,
        {'template_name':  'login.html'}, name='monitor_login'
    ),
    url(r'^meter/$', views.meters),
    url(r'^meter/add/$', views.add_meter),
    url(r'^meter/edit/$', views.edit_meter),
    url(r'^transformers/$', views.transformers),
    url(r'^transformers/add/$', views.add_transformers),
    url(r'^transformers/edit/$', views.edit_transformers),
    url(r'^user/$', views.users),
    url(r'^user/add/$', views.add_user),
    url(r'^user/edit/$', views.edit_user),
    url(r'^dashboard$', views.dashboard),
]
